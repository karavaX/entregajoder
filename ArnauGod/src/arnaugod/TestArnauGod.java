package arnaugod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestArnauGod {

	@Test
	void testPublic() {
		assertEquals(
				"{Lunnis6:4=Mataras a tu madre, Marcianito12:1=El misterio del universo que has visto, y de los ovnis relucientes: el universo es el blanco de mi sonrisa, y los ovnis su luz, Marcianito12:2=En cuanto vi la redondez de la Tierra, dude de ella, me lo dijo un Youtuber, MundoDesconocido1:6=Bienaventurado el teorista que rechaze la ciencia en favor de Youtube, MundoDesconocido3:2=Yo soy la luz, tu guia, tu salvacion, dice Arnau, tu futura redencion, la sonrisa, Pitagoras1:4=Mis calculos son, recursividad}\r\n"
						+ "\"Bienaventurado el teorista que rechaze la ciencia en favor de Youtube\"\r\n"
						+ "\"Yo soy la luz, tu guia, tu salvacion, dice Arnau, tu futura redencion, la sonrisa\"",
				ArnauGod.ArnauGod(6,
						"MundoDesconocido 1 6 Bienaventurado el teorista que rechaze la ciencia en favor de Youtube\r\n"
								+ "MundoDesconocido 3 2 Yo soy la luz, tu guia, tu salvacion, dice Arnau, tu futura redencion, la sonrisa\r\n"
								+ "Marcianito 12 2 En cuanto vi la redondez de la Tierra, dude de ella, me lo dijo un Youtuber\r\n"
								+ "Lunnis 6 4 Mataras a tu madre\r\n"
								+ "Marcianito 12 1 El misterio del universo que has visto, y de los ovnis relucientes: el universo es el blanco de mi sonrisa, y los ovnis su luz\r\n"
								+ "Pitagoras 1 4 Mis calculos son, recursividad",
						"mundo"));
		assertEquals(
				"{Marc1=Marc, siervo de Arnau, y fiel seguidor: Sonrisas y paz y amor os sean multiplicados, Marc4=Corred por la calles, pues eso es lo que quiere nuestro todopoderoso, aunque acabeis debajo de un chasis, Melon2:3=Es es como si por ejemplo, basicamente, haces esto, y te sale lo otro}\r\n"
						+ "\"Marc, siervo de Arnau, y fiel seguidor: Sonrisas y paz y amor os sean multiplicados\"\r\n"
						+ "\"Corred por la calles, pues eso es lo que quiere nuestro todopoderoso, aunque acabeis debajo de un chasis\"\r\n"
						+ "\"Es es como si por ejemplo, basicamente, haces esto, y te sale lo otro\"",
				ArnauGod.ArnauGod(3,
						"Marc 1 Marc, siervo de Arnau, y fiel seguidor: Sonrisas y paz y amor os sean multiplicados\r\n"
								+ "Marc 4 Corred por la calles, pues eso es lo que quiere nuestro todopoderoso, aunque acabeis debajo de un chasis\r\n"
								+ "Melon 2 3 Es es como si por ejemplo, basicamente, haces esto, y te sale lo otro",
						"m"));
	}

	@Test
	void testEasy() {
		assertEquals("{Ciencia1=Nadie debe, sin el consentimiento de la sonrisa, realizar ciencia, lmao12:6=Si}\r\n",
				ArnauGod.ArnauGod(2, "Ciencia 1 Nadie debe, sin el consentimiento de la sonrisa, realizar ciencia\r\n"
						+ "lmao 12 6 Si", "z"));
	}

	@Test
	void testHard() {
		assertEquals(
				"{Lmao1:1=Del molar derecho saco una aplicacion, lo llamo WebCasteller, Lmao3:1=Y de la nada, hitler, que descendia de una galaxia}\r\n"
						+ "\"Del molar derecho saco una aplicacion, lo llamo WebCasteller\"\r\n"
						+ "\"Y de la nada, hitler, que descendia de una galaxia\"",
				ArnauGod.ArnauGod(2, "Lmao 3 1 Y de la nada, hitler, que descendia de una galaxia\r\n"
						+ "Lmao 1 1 Del molar derecho saco una aplicacion, lo llamo WebCasteller", "lmao"));
	}

}
