package arnaugod;

import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;

public class ArnauGod {
	// Solucio del problema
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int cas = sc.nextInt();
		for (int i = 0; i < cas; i++) {
			// Create MAP
			TreeMap<String, String> episodes = new TreeMap<String, String>();
			// Introduce number of inputs
			int n = sc.nextInt();
			sc.nextLine();
			for (int j = 0; j < n; j++) {
				// Gets line words in array
				String[] episode = sc.nextLine().trim().split(" ");
				String episodeKey = episode[0] + episode[1];
				String episodeValue = "";
				// Checks is there's episode:verse or only episode
				if (Character.isDigit(episode[2].charAt(0))) {
					episodeKey += ":" + episode[2];
					for (int k = 3; k < episode.length; k++) {
						if (k + 1 != episode.length) {
							episodeValue += episode[k] + " ";
						} else {
							episodeValue += episode[k];
						}
					}
				} else {
					for (int k = 2; k < episode.length; k++) {
						if (k + 1 != episode.length) {
							episodeValue += episode[k] + " ";
						} else {
							episodeValue += episode[k];
						}
					}
				}
				episodes.put(episodeKey, episodeValue);
			}
			System.out.println(episodes);
			// Find if key contains the string
			Set<String> episodeSet = episodes.keySet();
			String keyPiece = sc.nextLine();
			ArrayList<String> keyMatch = new ArrayList<String>();
			for (String key : episodeSet) {
				if (key.toLowerCase().contains(keyPiece)) {
					keyMatch.add(key);
				}
			}
			for (int j = 0; j < keyMatch.size(); j++) {
				System.out.println("\""+episodes.get(keyMatch.get(j))+"\"");
			}
			System.out.println();
		}
	}
	
	// JUnit
	public static String ArnauGod(int n, String vers, String search) {
		TreeMap<String, String> episodes = new TreeMap<String, String>();
		ArrayList<String> lines = new ArrayList<>(Arrays.asList(vers.split("\r\n")));
		System.out.println(lines);
		String acc = "";
		for (int j = 0; j < n; j++) {
			String[] episode = lines.get(j).trim().split(" ");
			String episodeKey = episode[0] + episode[1];
			String episodeValue = "";
			if (Character.isDigit(episode[2].charAt(0))) {
				episodeKey += ":" + episode[2];
				for (int k = 3; k < episode.length; k++) {
					if (k + 1 != episode.length) {
						episodeValue += episode[k] + " ";
					} else {
						episodeValue += episode[k];
					}
				}
			} else {
				for (int k = 2; k < episode.length; k++) {
					if (k + 1 != episode.length) {
						episodeValue += episode[k] + " ";
					} else {
						episodeValue += episode[k];
					}
				}
			}
			episodes.put(episodeKey, episodeValue);
		}
		acc += episodes+"\r\n";
		Set<String> episodeSet = episodes.keySet();
		String keyPiece = search;
		ArrayList<String> keyMatch = new ArrayList<String>();
		for (String key : episodeSet) {
			if (key.toLowerCase().contains(keyPiece)) {
				keyMatch.add(key);
			}
		}
		for (int j = 0; j < keyMatch.size(); j++) {
			if(j+1 != keyMatch.size()) {
				acc += "\"" + episodes.get(keyMatch.get(j)) + "\"\r\n";
			} else {
				acc += "\"" + episodes.get(keyMatch.get(j)) + "\"";
			}
		}
		return acc;
	}
}