package minesweeper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
import Core.Board;
import Core.Window;

public class Minesweeper {

	// Global variables
	static Scanner sc = new Scanner(System.in);
	static Random rd = new Random();
	static Position ps = new Position();
	static Board b = new Board();
    static Window w = new Window(b);
	static ArrayList<String> winners = new ArrayList<>();
	static String player;
	static int rows = 8;
	static int cols = 8;
	static int nMines = 16;

	// Main execution
	public static void main(String[] args) {
		int[][] board = new int[rows][cols];
		int[][] mines = new int[rows][cols];
		// Menu
		int opt = -1;
		int optConf = -1;
		while (opt != 0) {
			printInterface();
			opt = sc.nextInt();
			sc.nextLine();
			switch (opt) {
			// Help
			case 1:
				printHelp();
				break;
			// Options
			case 2:
				options();
				break;
			// Play
			case 3:
				play(board, mines);
				break;
			// Winners
			case 4:
				printWinners();
				break;
			// Exit the game
			case 0:
				System.out.println("\n- Terminated -");
				break;
			// In case of severe brain damage
			default:
				System.out.println("\nPick a right option >:|\n");
			}
		}
	}
	
	/**
	 * Game loop, see comments inside
	 * @param board Board that is shown
	 * @param mines Hidden board with the mines
	 */
	private static void play(int[][] board, int[][] mines) {
		printSeparator();
		// Init board, mines and GUI
		board = initBoard(board);
		mines = initMines(mines, nMines);
		initGUI();
		printBoard(board);
		// Game loop
		boolean end = false;
		while (!end) {
			// Asks for position
			askPosition(board);
			// Uncovers the selected position
			uncover(ps.x, ps.y, board, mines);
			// Checks if the game is finished
			end = endGame(board, ps.x, ps.y);
			printSeparator();
			printBoard(board);
		}
		// Post-end, registers user if won
		finishedGame(board, mines);
	}
	
	/**
	 * Initializes the player's board
	 * @param board Board that is shown
	 * @return Player's board with covered boxes
	 */
	private static int[][] initBoard(int[][] board) {
		board = new int[rows][cols];
		// Cover all boxes
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				board[i][j] = 9;
			}
		}
		return board;
	}
	
	/**
	 * Initializes the mines board (hidden board), puts a number of mines in random locations
	 * @param mines Mines board (hidden board)
	 * @param n Number of mines
	 * @return Mines board (hidden) with n mines
	 */
	private static int[][] initMines(int[][] mines, int n) {
		mines = new int[rows][cols];
		for (int i = 0; i < n; i++) {
			// Random location
			int x = rd.nextInt(mines[0].length);
			int y = rd.nextInt(mines.length);
			// If there is a mine, generate another location
			while (mines[y][x] == 1) {
				x = rd.nextInt(mines[0].length);
				y = rd.nextInt(mines.length);
			}
			// Sets mine
			mines[y][x] = 1;
		}
		return mines;
	}
	
	/**
	 * Initializes the GUI
	 */
	private static void initGUI() {
		// Background color
		b.setColorbackground(0xb1adad);
		// Border
		b.setActborder(true);
		// Characters to show in base of numbers in board in order
		String[] output = { "", "1", "2", "3", "4", "5", "6", "7", "8", "█", "💣"};
		b.setText(output);
		// Colors for the characters in order
		int[] outputColor = { 0x0000FF, 0x00FF00, 0xFFFF00, 0xFF0000, 0xFF00FF, 0x00FFFF, 0x521b98, 0xFFFFFF, 0xFF8000, 0x7F00FF, 0x000000};
		b.setColortext(outputColor);
		// Label that bugs A LOT
		String[] labels = {"Mines: "+nMines};
		w.setLabels(labels);
		w.setActLabels(true);
		// Fancy window title
		w.setTitle("MineSweeper @karavaX");
	}
	
	/**
	 * Asks the player for a valid position
	 * @param board Board that is shown
	 */
	private static void askPosition(int[][] board) {
		System.out.print("\nCoord X > ");
		ps.x = sc.nextInt();
		System.out.print("Coord Y > ");
		ps.y = sc.nextInt();
		while (matrixOutOfBounds(board, ps.x, ps.y)) {
			System.out.println();
			System.out.println("Coords out of bounds");
			System.out.print("\nCoord X > ");
			ps.x = sc.nextInt();
			System.out.print("Coord Y > ");
			ps.y = sc.nextInt();
		}
	}
	
	/**
	 * Uncovers the selected box, if the number of mines around is 0, looks for the around boxes (recursive)
	 * @param x Position X (Horizontal) (column)
	 * @param y Position Y (Vertical) (row)
	 * @param board Player's board (shown)
	 * @param mines Mines board (hidden)
	 */
	private static void uncover(int x, int y, int[][] board, int[][] mines) {
		// [Base] If there's a mine
		if (checkPos(x, y, mines) != 0) {
			board[y][x] = checkPos(x, y, mines);
		} 
		// [Recursive] If there's no mine
		else {
			board[y][x] = 0;
			// top-left
			if (!matrixOutOfBounds(board, x - 1, y - 1) && board[y - 1][x - 1] != 0) {
				uncover(x - 1, y - 1, board, mines);
			}
			// top
			if (!matrixOutOfBounds(board, x, y - 1) && board[y - 1][x] != 0) {
				uncover(x, y - 1, board, mines);
			}
			// top-right
			if (!matrixOutOfBounds(board, x + 1, y - 1) && board[y - 1][x + 1] != 0) {
				uncover(x + 1, y - 1, board, mines);
			}
			// right
			if (!matrixOutOfBounds(board, x + 1, y) && board[y][x + 1] != 0) {
				uncover(x + 1, y, board, mines);
			}
			// bottom-right
			if (!matrixOutOfBounds(board, x + 1, y + 1) && board[y + 1][x + 1] != 0) {
				uncover(x + 1, y + 1, board, mines);
			}
			// bottom
			if (!matrixOutOfBounds(board, x, y + 1) && board[y + 1][x] != 0) {
				uncover(x, y + 1, board, mines);
			}
			// bottom-left
			if (!matrixOutOfBounds(board, x - 1, y + 1) && board[y + 1][x - 1] != 0) {
				uncover(x - 1, y + 1, board, mines);
			}
			// left
			if (!matrixOutOfBounds(board, x - 1, y) && board[y][x - 1] != 0) {
				uncover(x - 1, y, board, mines);
			}
		}

	}
	
	/**
	 * Checks the mines around a position (box)
	 * @param x Position X (Horizontal) (column)
	 * @param y Position Y (Vertical) (row)
	 * @param mines Mines board (hidden)
	 * @return Number of mines around
	 */
	private static int checkPos(int x, int y, int[][] mines) {
		if (mines[y][x] != 1) {
			ArrayList<Integer> values = matrixAround(mines, y, x);
			return Collections.frequency(values, 1);
		} else {
			// Returns the F*CKING D E A T H
			return 10;
		}
	}

	/**
	 * Checks if the game has ended
	 * @param board Player's board (shown)
	 * @param x Position X (Horizontal) (column)
	 * @param y Position Y (Vertical) (row)
	 * @return true=end, false=continue
	 */
	private static boolean endGame(int[][] board, int x, int y) {
		if (uncoveredBoxes(board) == nMines) {
			return true;
		} else if (board[y][x] == 10) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * How many uncovered boxes currently are
	 * @param board Player's board (shown)
	 * @return number of uncovered boxes
	 */
	private static int uncoveredBoxes(int[][] board) {
		int uncovered = 0;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == 9) {
					uncovered++;
				}
			}
		}
		return uncovered;
	}

	/**
	 * Checks if it's win/lose: if win, registers the player on winners, if lose, proceeds to cheer you up :)
	 * @param board Player's board (shown)
	 */
	private static void finishedGame(int[][] board, int[][] mines) {
		boolean win = true;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == 10) {
					win = false;
				}
			}
		}
		if (win) {
			if (!winners.contains(player) || !winners.contains("undefined")) {
				if (player == "") {
					winners.add("undefined");
				} else {
					winners.add(player);
				}
			}
			System.out.println("\nThats what I call a S O L I D victory\n");
			printBoardMines(board, mines);
		} else {
			System.out.println("\nYou suck, L O S E R\n");
			printBoardMines(board, mines);
		}
		printSeparator();
	}
	
	/**
	 * Takes the values of the positions around a specified coord. If working with [X,Y] you must switch x and y the values -> (matrix, y, x)
	 * @param matrix Matrix
	 * @param x Position X (ROWS!) (vertical)
	 * @param y Position Y (COLS!) (horizontal)
	 * @return List with around values
	 */
	public static ArrayList<Integer> matrixAround(int[][] matrix, int x, int y) {
		ArrayList<Integer> values = new ArrayList<Integer>();
		if (!matrixOutOfBounds(matrix, x - 1, y - 1)) {
			values.add(matrix[x - 1][y - 1]);
		}
		if (!matrixOutOfBounds(matrix, x - 1, y)) {
			values.add(matrix[x - 1][y]);
		}
		if (!matrixOutOfBounds(matrix, x - 1, y + 1)) {
			values.add(matrix[x - 1][y + 1]);
		}
		if (!matrixOutOfBounds(matrix, x, y + 1)) {
			values.add(matrix[x][y + 1]);
		}
		if (!matrixOutOfBounds(matrix, x + 1, y + 1)) {
			values.add(matrix[x + 1][y + 1]);
		}
		if (!matrixOutOfBounds(matrix, x + 1, y)) {
			values.add(matrix[x + 1][y]);
		}
		if (!matrixOutOfBounds(matrix, x + 1, y - 1)) {
			values.add(matrix[x + 1][y - 1]);
		}
		if (!matrixOutOfBounds(matrix, x, y - 1)) {
			values.add(matrix[x][y - 1]);
		}
		return values;
	}
	
	/**
	 * Checks if the given position is out of bounds
	 * @param matrix Matrix to measure
	 * @param x Position X (Horizontal) (column)
	 * @param y Position Y (Vertical) (row)
	 * @return true=Out of bounds, false=Inside matrix
	 */
	public static boolean matrixOutOfBounds(int[][] matrix, int x, int y) {
		if (x >= matrix[0].length || x < 0 || y < 0 || y >= matrix.length) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sets global options to customize the name, board and number of mines
	 */
	private static void options() {
		printSeparator();
		// Player's name
		System.out.print("Set player's name > ");
		player = sc.nextLine();
		// Board rows
		System.out.print("Set board rows > ");
		rows = sc.nextInt();
		// Board cols
		System.out.print("Set board cols > ");
		cols = sc.nextInt();
		// Mines
		System.out.print("Number of mines > ");
		nMines = sc.nextInt();
		while (nMines >= rows * cols || nMines <= 0) {
			System.out.println("\nImpossible number of mines!");
			System.out.print("\nNumber of mines > ");
			nMines = sc.nextInt();
		}
		printSeparator();
	}

	/**
	 * Prints the main menu
	 */
	private static void printInterface() {
		// Title
		System.out.print("MineSweeper 💣 @karavaX");
		// Name of players
		if (player == null) {
			System.out.print("\n\nSet player name!");
		} else {
			System.out.print("\n\nPlayer - " + player);
		}
		// Options
		System.out.print(
				"\n\n  1. Show help\n  2. Options\n  3. Play\n  4. Winner's list\n  0. Exit\n\nPick option [0-4]> ");
	}

	/**
	 * Prints the game help
	 */
	private static void printHelp() {
		printSeparator();
		System.out.println("## References in console");
		System.out.println("0 -> Empty box\n1-8 -> Number of mines around the box\n9 -> Uncovered box\n10 -> M I N E!");
		System.out.println("\n## References in window");
		System.out.println("1-8 -> Number of mines around the box\n█ -> Uncovered box\n💣 -> M I N E!");
		printSeparator();
	}

	/**
	 * Prints the list of winners
	 */
	private static void printWinners() {
		printSeparator();
		if (winners.isEmpty()) {
			System.out.println("Nobody won the game yet!");
		} else {
			System.out.println(winners);
		}
		printSeparator();
	}

	/**
	 * Interface separator, holy shit it's clean
	 */
	private static void printSeparator() {
		System.out.println("\n--------------------------------------------\n");
	}

	/**
	 * Prints the board in console with spaces and draws the matrix to the window
	 * @param a Matrix to print/draw
	 */
	private static void printBoard(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
		b.draw(a, 't');
	}
	
	/**
	 * Shows the board with all mine positions in console and window
	 * @param a Player's board (shown)
	 * @param c Mines board (hidden)
	 */
	private static void printBoardMines(int[][] a, int[][] c) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				if (c[i][j] == 1) {
					a[i][j] = 10;
				}
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
		b.draw(a, 't');
	}
}